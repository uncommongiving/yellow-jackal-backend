The task is to build a Django Rest Framework API that will be the data source for a customer ticketing application used by employees of an organization. You can use either a MySql or PostgreSQL datastore.

Security should use a modified version of built-in Django user security with JWT tokens. The user object should contain both name and email fields. When a user authenticates, a JWT token should be generated and passed back to the caller along with the user&#39;s id, name, email address. The token should be present in the Authorization header of all secure requests in the format &quot;Authorization: JWT {token\_string}&quot;

All endpoints except for the authorization endpoint require a valid JWT token.

Users are only created through the Django Admin interface; not through the API

Customer Fields

- id (PK)
- full\_name
- phone

Ticket Fields

- id (PK)
- customer\_id (FK to Customers table)
- contact\_name, (may or may not be customer&#39;s name)
- contact\_phone, (may or may not be customer&#39;s phone)
- rep\_id (FK to Users)
- created\_date (set during ticket creation),
- resolved\_date (should be populated when status is set to &#39;Resolved&#39;)
- last\_modified\_date (autoupdate)
- subject
- details
- status (set to &#39;pending&#39; for new tickets, possible values are pending, ready for approval, resolved, reopened)

Note Fields

- id (pk)
- ticket\_id (FK to Ticket)
- rep\_id (FK to users)
- note\_text
- created\_date

Action Item Fields

- id (pk)
- rep\_id (FK to users)
- created\_by (FK to users)
- description
- due\_date
- is\_complete

Use pagination for all list-based queries.

APIs needed:

- Get Users (return id, name, email)
- Get Call Summary (returns number of calls at each status)
- Get Action Items for logged in user / status (returns a list of due date / description / is\_complete for all action items associated with the user from the JWT token) - optionally can be filtered by is\_complete=true or false
- Create Action Item (takes rep\_id for whom the action item will be created, description, due date, autopopulates created\_by with the id of the user on the JWT token)
- Resolve Action Item (takes item id, sets is\_complete=true)
- Get Tickets (optional date range filter, optional status filter that can contain one or multiple statuses, if filter not supplied return all), returns all ticket fields, paged
- Get Ticket Detail by ID (returns ticket detail (ticket id, customer id, contact name, contact phone, rep id &amp; rep name, subject, details, open date, resolved date, status) + all notes (id, datetime, rep name &amp; id, text) for the ticket -- no pagination on the notes)
- Get Customer by ID (returns Customer ID, Customer Name, Phone number)
- Add Ticket (takes customer id, contact name, contact phone, subject, details, autopopulate datetime &amp; rep id from the JWT logged in user)
- Update Ticket Status (takes ticket ID, new status - autopopulate lastmod time)
- Add Note To Ticket (takes ticket id, note text - autopopulate datetime &amp; rep id from the JWT logged in user)